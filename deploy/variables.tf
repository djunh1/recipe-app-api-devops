variable "prefix" {
  default = "gametime"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "djunh1@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for Postgres RDS instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-keypair"
}

variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "388527682786.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for proxy"
  default     = "388527682786.dkr.ecr.us-east-1.amazonaws.com/recipie-api-app-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for app"
}
